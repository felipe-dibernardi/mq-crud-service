package br.com.fdbst.mqcrud.controller;

import br.com.fdbst.mqcrud.model.User;
import br.com.fdbst.mqcrud.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @PostMapping("/")
    public ResponseEntity createNewUser(@RequestBody User user) {
        userService.createNewUser(user);
        return ResponseEntity.ok().build();
    }

}
