package br.com.fdbst.mqcrud.messaging;

import br.com.fdbst.mqcrud.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class Producer {

    private final RabbitTemplate rabbitTemplate;

    @Value("topic.exchange.new-user")
    private String newUserTopicExchange;

    @Value("routing.key.new-user.monthly-fee")
    private String newUserMonthlyFeeRoutingKey;

    public void sendNewUser(User user) {
        System.out.println("Sending message...");
        rabbitTemplate.convertAndSend(newUserTopicExchange, newUserMonthlyFeeRoutingKey, user.toString());
    }

}
