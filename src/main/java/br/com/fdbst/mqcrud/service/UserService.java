package br.com.fdbst.mqcrud.service;

import br.com.fdbst.mqcrud.messaging.Producer;
import br.com.fdbst.mqcrud.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserService {

    private final Producer producer;

    public void createNewUser(User user) {
        producer.sendNewUser(user);
    }

}
