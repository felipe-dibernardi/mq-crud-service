package br.com.fdbst.mqcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqCrudApplication.class, args);
    }

}
